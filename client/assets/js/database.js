let token = localStorage.getItem("token");
let isAdmin = localStorage.getItem("isAdmin");
let usersTable = document.getElementById("usersTable");
let coursesTable = document.getElementById("coursesTable");
if(isAdmin == "true") {
	fetch("http://localhost:3000/api/users/",{
		method : "GET",
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		}
	})
	.then(res => res.json())
	.then(data => {
		let userProfile;
		for (userProfile of data) {
			if(userProfile.isAdmin === false){
				usersTable.innerHTML +=
				`
					<tr>
						<td>
							${userProfile.firstName}
						</td>
						<td>
							${userProfile.lastName}
						</td>
						<td>
							${userProfile.email}
						</td>
						<td>
							${userProfile.mobileNo}
						</td>
					</tr>
				`
			}else{
				continue;
			}
		}
	})
	fetch("http://localhost:3000/api/courses/",{
		method : "GET",
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		}
	})
	.then(res => res.json())
	.then(data => {
		let courseProfile;
		for (courseProfile of data) {
			let status;
			if(courseProfile.isActive === true){
				status = "ENABLED"
			} else {
				status = "DISABLED"
			}
			coursesTable.innerHTML +=
			`
				<tr>
					<td>
						${courseProfile.name}
					</td>
					<td>
						${courseProfile.description}
					</td>
					<td>
						${courseProfile.price}
					</td>
					<td>
						${status}
					</td>
					<td>
						${courseProfile.createdOn}
					</td>
				</tr>
			`
		}
	})
}