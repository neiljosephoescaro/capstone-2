let params = new URLSearchParams(window.location.search);
let courseId = params.get("courseId");
let token = localStorage.getItem("token");

fetch(`http://localhost:3000/api/courses/${courseId}`, {
	method : "DELETE",
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
	}
})
.then( res => res.json())
.then( data => {
	if (data === true) {
		window.location.replace("./courses.html");
	}
})